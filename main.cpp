#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <errno.h>
#include <linux/major.h>
#include <scsi/sg.h>
#include <scsi/scsi.h>
#include <string.h>
#include <set>

const size_t CD_SECTOR_SIZE = 2352;

struct sg_id {
    long l1;
    long l2;
};

uint8_t swap_endianness(uint8_t value) {
    return value;
}

uint16_t swap_endianness(uint16_t value) {
    return static_cast<uint16_t>(((value & 0x00'FF) << 8) | (value >> 8));
}

uint32_t swap_endianness(uint32_t value) {
    return ((value & 0x00'00'00'FF) << 24) |
            ((value & 0x00'00'FF'00) << 8) |
            ((value & 0x00'FF'00'00) >> 8) |
            ((value & 0xFF'00'00'00) >> 24);
}

struct inquiry_standard_data {
    // First Byte
    unsigned int peripheral_device_type : 5;
    unsigned int peripheral_qualifier : 3;
    // Second Byte
    unsigned int first_reserved_block : 7;
    bool removable_media : 1;
    // Third Byte
    unsigned int version : 8;
    // Fourth Byte
    unsigned int response_data_format : 4;
    bool normal_aca_support : 1;
    bool hierarchical_support : 1;
    unsigned int first_obsolete_block : 2;
    // Fifth Byte
    unsigned int additional_length : 8;
    // Sixth Byte
    bool protect : 1;
    unsigned int second_reserved_block: 2;
    bool _3pc : 1;
    unsigned int tpgs : 2;
    bool acc : 1;
    bool sccs : 1;
    // Seventh byte
    unsigned int seventh_byte : 8;
    // Eighth byte
    unsigned int eighth_byte : 8;
    // 9th - 16th bytes
    char vendor_identification[8];
    // 17th - 32nd bytes
    char product_identification[16];
    // 33rd - 36th bytes
    char product_revision_level[4];
    // Misc data
    unsigned char misc[60];
};

enum class page_control : uint8_t {
    current_values,
    changeable_values,
    default_values,
    saved_values
};

struct mode_sense_request {
    uint8_t operation_code;
    uint8_t first_reserved_block : 3;
    bool disable_block_descriptors : 1;
    bool long_lba_accepted : 1;
    uint8_t second_reserved_block : 3;
    uint8_t page_code : 6;
    uint8_t page_control : 2;
    uint8_t subpage_code;
    uint32_t third_reserved_block : 24;
    uint16_t allocation_length;
    uint8_t control;
} __attribute__ ((packed));

struct read_toc_pma_atip_request {
    uint8_t operation_code;
    uint8_t first_reserved_block : 1;
    bool msf : 1;
    uint8_t second_reserved_block : 6;
    uint8_t format : 4;
    uint8_t third_reserved_block: 4;
    uint8_t fourth_reserved_block;
    uint8_t fifth_reserved_block;
    uint8_t sixth_reserved_block;
    union {
        uint8_t track_number;
        uint8_t session_number;
    };
    uint16_t allocation_length;
    uint8_t control;
} __attribute__ ((packed));

struct minute_second_frame {
    uint8_t reserved;
    uint8_t minutes;
    uint8_t seconds;
    uint8_t frames;
};

struct read_toc_response_track_descriptor {
    uint8_t first_reserved_block;
    uint8_t control : 4;
    uint8_t adr : 4;
    uint8_t track_number;
    uint8_t reserved;
    union {
        minute_second_frame msf;
        uint32_t logical_block_address;
    };
};

struct read_toc_response {
    uint16_t data_length;
    uint8_t first_track_number;
    uint8_t last_track_number;
    read_toc_response_track_descriptor track_data[];
};

struct read_cd_text_response {
    uint16_t size_in_bytes;
    uint16_t first_reserved_block;
    uint8_t data[];
};

struct read_cd_request {
    // Byte 0
    uint8_t operation_code;
    // Byte 1
    bool reladr : 1;
    uint8_t first_reserved_block : 1;
    uint8_t sector_type : 3;
    uint8_t second_reserved_block : 3;
    // Byte 2, 3, 4, 5
    uint32_t starting_logical_block_address;
    // Byte 6, 7, 8
    uint32_t transfer_length_in_blocks : 24;
    // Byte 9
    uint8_t third_reserved_block : 1;
    uint8_t error_field : 2;
    bool edc_or_ecc : 1;
    bool user_data : 1;
    uint8_t header_codes : 2;
    bool sync : 1;
    // Byte 10
    uint8_t subchannel_selection_bits : 3;
    uint8_t fourth_reserved_block : 5;
    // Byte 11
    uint8_t control;
} __attribute__ ((packed));

void print_binary_data(const uint8_t* data, size_t length, size_t numberOfBytesToShow) {
    printf("---------");
    for (int i = 0; i < numberOfBytesToShow; i++) {
        printf("%02X ", i);
    }
    printf("\n");
    int i = 0;
    printf("%08X ", i);
    while (i < length) {
        printf("%02X ", data[i]);
        ++i;
        if (i % numberOfBytesToShow == 0) {
            printf("\n%08X ", i);
        }
    }
    printf("\n");
}

void print_centered_text(const char* const text, size_t numberOfColumns) {
    auto textLength = strlen(text);
    auto otherColumnCount = (numberOfColumns - textLength) / 2;
    for (int i = 0; i < otherColumnCount; ++i) {
        printf("-");
    }
    printf(text);
    for (int i = 0; i < otherColumnCount; ++i) {
        printf("-");
    }
    printf("\n");
}

int main() {
    winsize size;
    if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &size) == -1) {
        fprintf(stderr, "Could not get terminal size\n");
        return -1;
    }
    uint32_t numberOfBytesToShow = ((size.ws_col - 9) / 4 / 16) * 16;

    struct stat st;
    if (stat("/dev/cdrom", &st)) {
        fprintf(stderr, "Could not stat cd-rom\n");
        return -1;
    }
    print_centered_text("Drive Status Information", size.ws_col);
    printf("Block size: %d\n", st.st_blksize);
    printf("Blocks: %d\n", st.st_blocks);
    auto type = static_cast<int>(st.st_rdev >> 8);
    switch (type) {
        case IDE0_MAJOR:
        case IDE1_MAJOR:
        case IDE2_MAJOR:
        case IDE3_MAJOR:
            printf("IDE 1/2/3... requires ping\n");
            break;
        case CDU31A_CDROM_MAJOR:
            printf("Sony CDU31A or compatible\n");
            break;
        case CDU535_CDROM_MAJOR:
            printf("Sony CDU535 or compatible\n");
            break;
        case MATSUSHITA_CDROM_MAJOR:
        case MATSUSHITA_CDROM2_MAJOR:
        case MATSUSHITA_CDROM3_MAJOR:
        case MATSUSHITA_CDROM4_MAJOR:
            printf("non-ATAPI IDE-style Matsushita/Panasonic CR-5xx or compatible\n");
            break;
        case SANYO_CDROM_MAJOR:
            printf("Sanyo proprietary or compatible: NOT CDDA CAPABLE\n");
            break;
        case MITSUMI_CDROM_MAJOR:
        case MITSUMI_X_CDROM_MAJOR:
            printf("Mitsumi proprietary or compatible: NOT CDDA CAPABLE\n");
            break;
        case OPTICS_CDROM_MAJOR:
            printf("Optics Dolphin or compatible; NOT CDDA CAPABLE\n");
            break;
        case AZTECH_CDROM_MAJOR:
            printf("Aztech proprietary or compatbile: NOT CDDA CAPABLE\n");
            break;
        case GOLDSTAR_CDROM_MAJOR:
            printf("Goldstar\n");
            break;
        case CM206_CDROM_MAJOR:
            printf("Phillips/LMS CM206 or compatible\n");
            break;
        case SCSI_CDROM_MAJOR:
            printf("SCSI\n");
            break;
        case SCSI_GENERIC_MAJOR:
            printf("SCSI Generic\n");
            break;
        default:
            printf("Something else...\n");

    }

    uint8_t buffer[CD_SECTOR_SIZE];
    int fd = open("/dev/cdrom", O_RDONLY | O_LARGEFILE | O_NONBLOCK);
    if (fd == -1) {
        fprintf(stderr, "Could not open the CD drive\n");
        return -1;
    }
    sg_id argid;
    if (ioctl(fd, SCSI_IOCTL_GET_IDLUN, &argid)) {
        fprintf(stderr, "Could not get scsi id information\n");
        close(fd);
        return -1;
    }
    int bus = argid.l2;
    int id = argid.l1 & 0xFF;
    int lun = (argid.l1 >> 8) & 0xFF;
    printf("Bus = %d; id = %d; lun = %d;\n", bus, id, lun);

    close(fd);

    fd = open("/dev/sr0", O_RDWR | O_NONBLOCK);
    if (fd < 0) {
        fprintf(stderr, "Could not open device for read-write\n");
        return -1;
    }
    print_centered_text("SG I/O Access", size.ws_col);
    sg_io_hdr hdr;
    memset(&hdr, 0, sizeof(sg_io_hdr));
    hdr.interface_id = 'A';
    if (ioctl(fd, SG_IO, &hdr)) {
        switch (errno) {
            case EINVAL:
            case ENOSYS:
                printf("Allows SG I/O access\n");
                break;
            default:
                printf("Does not allow SG I/O access\n");
                close(fd);
                return 0;
        }
    } else {
        printf("Does not allow SG I/O access\n");
        close(fd);
        return 0;
    }

    print_centered_text("Standard Data Inqury", size.ws_col);
    inquiry_standard_data inquiryStandardData = { 0 };
    unsigned char sense[SG_MAX_SENSE];
    unsigned char command[6] = { 0x12, 0, 0, 0, 56, 0 };
    memset(sense, 0, SG_MAX_SENSE);
    memset(&hdr, 0, sizeof(sg_io_hdr));
    hdr.flags = SG_FLAG_DIRECT_IO;
    hdr.cmdp = command;
    hdr.cmd_len = 6;
    hdr.sbp = sense;
    hdr.mx_sb_len = SG_MAX_SENSE;
    hdr.interface_id = 'S';
    hdr.timeout = 50'000;
    hdr.dxferp = &inquiryStandardData;
    hdr.dxfer_len = sizeof(inquiry_standard_data);
    hdr.dxfer_direction = SG_DXFER_TO_FROM_DEV;

    auto status = ioctl(fd, SG_IO, &hdr);
    printf("status: %d\n", status);
    printf("hdr.status: %d\n", hdr.status);
    printf("size of struct: %zu\n", sizeof(inquiry_standard_data));
    printf("peripheral qualifier: %x\n", inquiryStandardData.peripheral_qualifier);
    printf("peripheral device type: %x\n", inquiryStandardData.peripheral_device_type);
    printf("is removable media: %s\n", inquiryStandardData.removable_media ? "yes" : "no");
    printf("version: %02x\n", inquiryStandardData.version);
    printf("NORMACA: %x\n", inquiryStandardData.normal_aca_support);
    printf("HISUP: %x\n", inquiryStandardData.hierarchical_support);
    printf("Response data format: %x\n", inquiryStandardData.response_data_format);
    printf("Additional length: %d\n", inquiryStandardData.additional_length);
    printf("SCCS: %x\n", inquiryStandardData.sccs);
    printf("ACC: %x\n", inquiryStandardData.acc);
    printf("TPGS: %x\n", inquiryStandardData.tpgs);
    printf("3PC: %x\n", inquiryStandardData._3pc);
    printf("Protect: %x\n", inquiryStandardData.protect);
    printf("Vendor Identification: '%.8s'\n", inquiryStandardData.vendor_identification);
    printf("Product Identification: '%.16s'\n", inquiryStandardData.product_identification);
    printf("Product Revision Level: '%.4s'\n", inquiryStandardData.product_revision_level);
    printf("37th byte: %02x\n", inquiryStandardData.misc[0]);

    print_centered_text("ATAPI", size.ws_col);
    int atapi_value;
    if (ioctl(fd, SG_EMULATED_HOST, &atapi_value)) {
        fprintf(stderr, "Could not check for emulated host type\n");
    } else {
        printf("IS ATA: %s\n", (atapi_value ? "yes" : "no"));
    }


    // region Mode Sense
    const size_t DATA_LENGTH = 26;
    mode_sense_request modeSenseRequest = { 0 };
    modeSenseRequest.operation_code = 0x5A;
    modeSenseRequest.page_code = 0x01;
    modeSenseRequest.page_control = static_cast<uint8_t>(page_control::current_values);
    modeSenseRequest.allocation_length = swap_endianness(static_cast<uint16_t>(DATA_LENGTH));

    uint8_t modeSenseData[DATA_LENGTH];
    memset(modeSenseData, 0, DATA_LENGTH);
    memset(sense, 0, SG_MAX_SENSE);
    memset(&hdr, 0, sizeof(sg_io_hdr));
    hdr.flags = SG_FLAG_DIRECT_IO;
    hdr.cmdp = reinterpret_cast<unsigned char*>(&modeSenseRequest);
    hdr.cmd_len = sizeof(mode_sense_request);
    hdr.sbp = sense;
    hdr.mx_sb_len = SG_MAX_SENSE;
    hdr.interface_id = 'S';
    hdr.timeout = 120'000;
    hdr.dxferp = modeSenseData;
    hdr.dxfer_len = DATA_LENGTH;
    hdr.dxfer_direction = SG_DXFER_TO_FROM_DEV;
    print_centered_text("Mode Sense", size.ws_col);
    print_binary_data(reinterpret_cast<unsigned char*>(&modeSenseRequest), sizeof(mode_sense_request), numberOfBytesToShow);
    if (ioctl(fd, SG_IO, &hdr) || hdr.status != 0) {
        fprintf(stderr, "Could not execute mode sense command\n");
        fprintf(stderr, "Status flag: %u\n", hdr.status);
    } else {
        printf("Successfully executed mode sense command\n");
        print_binary_data(modeSenseData, DATA_LENGTH, numberOfBytesToShow);
    }
    // endregion

    // region Table of Contents
    print_centered_text("Table of Contents", size.ws_col);
    printf("sizeof(read_toc_pma_atip_request): %zu\n", sizeof(read_toc_pma_atip_request));
    printf("sizeof(read_toc_response_track_descriptor): %zu\n", sizeof(read_toc_response_track_descriptor));
    printf("sizeof(read_toc_response): %zu\n", sizeof(read_toc_response));
    read_toc_pma_atip_request toc_request = { 0 };
    toc_request.operation_code = 0x43;
    toc_request.track_number = 1;
    toc_request.allocation_length = swap_endianness(static_cast<uint16_t>(1024));
    toc_request.msf = true;
    uint8_t tocData[1024];
    memset(tocData, 0, 1024);
    memset(sense, 0, SG_MAX_SENSE);
    memset(&hdr, 0, sizeof(sg_io_hdr));
    hdr.flags = SG_FLAG_DIRECT_IO;
    hdr.cmdp = reinterpret_cast<unsigned char*>(&toc_request);
    hdr.cmd_len = sizeof(read_toc_pma_atip_request);
    hdr.sbp = sense;
    hdr.mx_sb_len = SG_MAX_SENSE;
    hdr.interface_id = 'S';
    hdr.timeout = 120'000;
    hdr.dxferp = tocData;
    hdr.dxfer_len = 1024;
    hdr.dxfer_direction = SG_DXFER_TO_FROM_DEV;
    printf("---------");
    for (int i = 0; i < numberOfBytesToShow; i++) {
        printf("%02X ", i);
    }
    printf("\n");
    int i = 0;
    printf("%08X ", 0);
    while (i < 10) {
        printf("%02X ", hdr.cmdp[i]);
        ++i;
        if (i % numberOfBytesToShow == 0) {
            printf("\n%08X ", i);
        }
    }
    printf("\n");
    if (ioctl(fd, SG_IO, &hdr) || hdr.status != 0) {
        fprintf(stderr, "Could not get TOC\n");
    } else {
        auto response = reinterpret_cast<read_toc_response*>(tocData);
        printf("data_length: %u\n", swap_endianness(response->data_length));
        printf("first track number: %u\n", response->first_track_number);
        printf("last track number: %u\n", response->last_track_number);
        printf("Control | ADR | Track Number | Address   | MM:SS:FF |\n");
        printf("--------|-----|--------------|-----------|----------|\n");
        for (int current_track = 0; current_track < response->last_track_number - response->first_track_number + 1; ++current_track) {
            read_toc_response_track_descriptor& descriptor = response->track_data[current_track];
            printf("%7d | ", descriptor.control);
            printf("%3d | ", descriptor.adr);
            printf("%12u | ", descriptor.track_number);
            printf("%08Xh | ", descriptor.msf.frames + descriptor.msf.seconds * 75 + descriptor.msf.minutes * 60 * 75 - 150);
            printf("%02d:%02d:%02d |\n", descriptor.msf.minutes, descriptor.msf.seconds, descriptor.msf.frames);
        }

        toc_request.track_number = 0xAA;
        if (!ioctl(fd, SG_IO, &hdr) && hdr.status == 0) {
            response = reinterpret_cast<read_toc_response*>(tocData);
            auto leadOutTrack = response->track_data[0];
            printf("%7d | ", leadOutTrack.control);
            printf("%3d | ", leadOutTrack.adr);
            printf("%12s | ", "(LEAD OUT)");
            printf("%08Xh | ", leadOutTrack.msf.frames + leadOutTrack.msf.seconds * 75 + leadOutTrack.msf.minutes * 60 * 75 - 150);
            printf("%02d:%02d:%02d |\n", leadOutTrack.msf.minutes, leadOutTrack.msf.seconds, leadOutTrack.msf.frames);
        }
    }
    // endregion

    // region CD Text
    auto const READ_CD_TEXT_BUFFER = 1'024;
    print_centered_text("CD Text", size.ws_col);
    read_toc_pma_atip_request cd_text_request = { 0 };
    cd_text_request.operation_code = 0x43;
    cd_text_request.format = 0x05;
    cd_text_request.allocation_length = swap_endianness(static_cast<uint16_t>(READ_CD_TEXT_BUFFER));
    print_binary_data(reinterpret_cast<uint8_t*>(&cd_text_request), sizeof(cd_text_request), numberOfBytesToShow);
    uint8_t cdTextData[READ_CD_TEXT_BUFFER];
    memset(cdTextData, 0, READ_CD_TEXT_BUFFER);
    memset(sense, 0, SG_MAX_SENSE);
    memset(&hdr, 0, sizeof(sg_io_hdr));
    hdr.flags = SG_FLAG_DIRECT_IO;
    hdr.cmdp = reinterpret_cast<unsigned char*>(&cd_text_request);
    hdr.cmd_len = sizeof(read_toc_pma_atip_request);
    hdr.sbp = sense;
    hdr.mx_sb_len = SG_MAX_SENSE;
    hdr.interface_id = 'S';
    hdr.timeout = 120'000;
    hdr.dxferp = cdTextData;
    hdr.dxfer_len = READ_CD_TEXT_BUFFER;
    hdr.dxfer_direction = SG_DXFER_TO_FROM_DEV;

    printf("\n");
    if (ioctl(fd, SG_IO, &hdr) || hdr.status != 0) {
        fprintf(stderr, "Could not get CD Text\n");
    } else {
        printf("Got CD Text\n");
        auto response = reinterpret_cast<read_cd_text_response*>(cdTextData);
        uint16_t size_of_struct = swap_endianness(response->size_in_bytes);
        printf("CD Text response size: %d\n", size_of_struct);
        print_binary_data(cdTextData, READ_CD_TEXT_BUFFER, numberOfBytesToShow);
    }
    // endregion

    // region Read Something
    read_cd_request readCdRequest = { 0 };
    readCdRequest.operation_code = 0xBE;
    readCdRequest.starting_logical_block_address = 0; //swap_endianness(140U);
    readCdRequest.transfer_length_in_blocks = 1 << 16;
    readCdRequest.sync = true;
    readCdRequest.header_codes = 3;
    readCdRequest.user_data = true;
    readCdRequest.edc_or_ecc = true;
    print_binary_data(reinterpret_cast<uint8_t*>(&readCdRequest), sizeof(read_cd_request), numberOfBytesToShow);

    const int READ_SIZE = 2360;
    uint8_t readData[READ_SIZE];
    memset(readData, '\177', READ_SIZE);
    memset(sense, 0, SG_MAX_SENSE);
    memset(&hdr, 0, sizeof(sg_io_hdr));
    hdr.flags = SG_FLAG_DIRECT_IO;
    hdr.cmdp = reinterpret_cast<unsigned char*>(&readCdRequest);
    hdr.cmd_len = sizeof(read_cd_request);
    hdr.sbp = sense;
    hdr.mx_sb_len = SG_MAX_SENSE;
    hdr.interface_id = 'S';
    hdr.timeout = 120'000;
    hdr.dxferp = readData;
    hdr.dxfer_len = READ_SIZE;
    hdr.dxfer_direction = SG_DXFER_TO_FROM_DEV;
    if (ioctl(fd, SG_IO, &hdr) && hdr.status == 0) {
        fprintf(stderr, "Could not read sector\n");
    } else {
        printf("Successfully read first sector\n");
        print_binary_data(readData, READ_SIZE, numberOfBytesToShow);
    }
    // endregion

    close(fd);
    return 0;
}